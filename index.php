<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilo.css">
    <title>Formulario de inscripción MKT</title>
</head>
<body>
        <div class="contenedor">
            <form class="formulario" method="POST">
                <div>
                    <img src="logo.jpg" alt="">
                </div>
                <div>
                    <label for="name">
                    <p>Nombre y Apellidos</p> 
                    <input type="text" name="user_nam" required>
                    </label>
                </div>

                <div>
                    <label for="DNI">
                        <p>DNI</p> 
                        <input type="text" name="dni" required>
                    </label>
                </div>

                <!-- Correo electrónico -->
                <div>
                    <label for="">
                    <p>Correo</p> 
                    <input type="text" name="mail" required>
                    </label>
                </div>

                <!-- Número -->
                <div>
                    <label for="">
                        <p>número de contacto</p> 
                        <input type="text" name="cel" maxlength="9" required>
                    </label>
                </div>

                

                <!-- Horarios -->    
                <div class="horarios">
                    <h3>HORARIOS</h3>
                    <label for="horario">
                        <input type="checkbox" name="horario[]" value="lmv">
                        <b>LUNES - MIERCOLES - VIERNES</b> / 7:00 PM  a 9:00 PM
                        <input type="checkbox" name="horario[]" value="jvs">
                        <b>MARTES - JUEVES - SABADOS</b> / 7:00 PM a 9:00 PM
                    </label>
                </div>

                <!-- Inicio de curso -->
                <div class="curso_init">
                    <h3>INICIO DE CURSO</h3>
                    <label for="inicio">
                        <input type="checkbox" name="inicio[]" value="trece_13">
                        LUNES 13 DE FEBRERO
                        <input type="checkbox" name="inicio[]" value="catorce_14">
                        MARTES 14 DE FEBRERO
                    </label>
                </div>

                <!-- Metodo de pago -->
                <div>
                    <h3>Método de pago</h3>
                    <label>
                        <input type="radio" name="pago" value="depósito" >
                        Depósito a cuenta
                        <input type="radio" name="pago" value="transferencia" >
                        Transferencia
                        <input type="radio" name="pago" value="yape" >
                        Yape
                    </label>
                    
                </div>

                <hr>

                <!-- cuentas -->
                <div class="cuentas">
                    <h3>CUENTAS:</h3>
                    <h4><b>CENTRO ESPECIALIZADO EN ADIESTRAMIENTO DE REDES Y TECNOLOGÍAS DE LA INFORMACION SAC:</b></h4>
                    <p><b>BCP</b> Soles</p> 
                    <p>193-99923971-0-01</p>
                    <p><b>N° CUENTA INTERBANCARIA</b></p> <p>002 193 1 9992397100117</p>
                    <h4>YAPE : CENTRO ESPECIALIZADO EN SOPORTE Y ADISENTRAMIENTO EN REDES DE TECNOLOGIAS</h4>
                    <p>999 889 109</p>
                </div>

                <hr>

                <div class="boleta">
                    <label for="">
                        <h3>Sube tu voucher</h3>
                        <input type="file" name="upload" >
                    </label>
                </div>

                <hr>

                <!-- Dirección -->
                <div>
                    <h3>DIRECCIÓN</h3>
                    <p>AV. CANADA 3999 SEGUNDO PISO - CRUCE CON CIRCUNVALACION</p>
                    <p>CONTACTO : 924 780 044 / 934 526 433</p> 
                </div>

                <div>
                    <input type="submit" name="register">
                </div>
                
            </form>
        </div>
        <?php
        include("formu.php"); 
         ?>
    
</body>
</html>